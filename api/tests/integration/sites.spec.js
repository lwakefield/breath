/* eslint-env jest */
const fetch = require('node-fetch')

const getAuthHeader = require('./auth')

const {
    User,
    Profile,
    Site,
    Schedule
} = require('db')

describe('GET /sites', () => {
    it('gets empty list', async () => {
        const res = await fetch(`${API_URL}/sites`)
        expect(res.status).toEqual(200)
        expect(await res.json()).toEqual([])
    })
    it('gets non empty list', async () => {
        const models = await Site.query().insert([
            {url: 'foo'},
            {url: 'bar'},
            {url: 'baz'},
        ])

        const res = await fetch(`${API_URL}/sites`)
        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.length).toEqual(3)
    })
})

describe('GET /sites/:id', () => {
    it('fetches relevant model', async () => {
        const user = await User.create()
        const models = await Site.query().insert([
            {url: 'foo', user_id: user.id},
            {url: 'bar', user_id: user.id},
            {url: 'baz', user_id: user.id},
        ])

        const res = await fetch(
            `${API_URL}/sites/${models[1].id}`,
            { headers: getAuthHeader(user.id) }
        )
        expect(res.status).toEqual(200)
        const data = await res.json()

        expect(data).toBeDefined()
        expect(data.id).toEqual(models[1].id)
    })
    it('401s for an incorrect owner', async () => {
        const user = await User.create()
        const models = await Site.query().insert([
            {url: 'foo', user_id: user.id},
            {url: 'bar', user_id: user.id},
            {url: 'baz', user_id: user.id},
        ])

        const res = await fetch(
            `${API_URL}/sites/${models[1].id}`,
            { headers: getAuthHeader(user.id + 1) }
        )
        expect(res.status).toEqual(401)
    })
    it('404s correctly', async () => {
        const user = await User.create()

        const res = await fetch(
            `${API_URL}/sites/123`,
            { headers: getAuthHeader(user.id) }
        )
        expect(res.status).toEqual(404)
        expect(await res.text()).toEqual('Not Found')
    })
})

describe('POST /users/:userId/sites', () => {
    it('creates a site', async () => {
        const user = await User.query().insert({})

        const body = JSON.stringify({url: 'http://foo.com'})
        const headers = Object.assign(
            {'Content-Type': 'application/json'},
            getAuthHeader(user.id)
        )

        const res = await fetch(
            `${API_URL}/users/${user.id}/sites`,
            {
                method: 'POST',
                body,
                headers
            }
        )
        expect(res.status).toEqual(200)
        const data = await res.json()
        const properties = [
            'id',
            'url',
            'created_at',
        ]
        properties.forEach(p => expect(data).toHaveProperty(p))
        expect(data.url).toEqual('http://foo.com')

        const userWithSite = (
            await User.query().eager('[sites, sites.schedules]').where('id', user.id)
        )[0]
        expect(userWithSite).toBeDefined()
        expect(userWithSite.sites.length).toEqual(1)
        expect(userWithSite.sites[0].schedules.length).toEqual(1)
    })
    it('receives 401 without auth', async () => {
        const body = JSON.stringify({url: 'http://foo.com'})
        const headers = {
            'Content-Type': 'application/json'
        }

        const res = await fetch(
            `${API_URL}/users/1/sites`,
            {
                method: 'POST',
                body,
                headers
            }
        )
        expect(res.status).toEqual(401)
    })
    it('receives 401 without auth for wrong user', async () => {
        const body = JSON.stringify({url: 'http://foo.com'})
        const headers = Object.assign(
            {'Content-Type': 'application/json'},
            getAuthHeader(2)
        )

        const res = await fetch(
            `${API_URL}/users/1/sites`,
            {
                method: 'POST',
                body,
                headers
            }
        )
        expect(res.status).toEqual(401)
    })
})
