const uuid = require('uuid/v4')
const fetch = require('node-fetch')
const kue = require('kue')

const {
    User,
    Profile,
    Site,
    Schedule
} = require('db')

const getAuthHeader = require('./auth')

describe('GET /sites/:id/profiles', () => {
    it('gets empty list', async () => {
        const user = await User.create()
        const site = await Site.create(
            {url: 'foo', user_id: user.id}
        )

        const res = await fetch(
            `${API_URL}/sites/${site.id}/profiles`,
            {headers: getAuthHeader(user.id)}
        )

        expect(res.status).toEqual(200)
        expect(await res.json()).toEqual([])
    })

    it('gets 403 if user doesn\'t own site', async () => {
        const [alice, eve] = await User.create([{}, {}])
        const site = await Site.create(
            {url: 'foo', user_id: alice.id}
        )

        const res = await fetch(
            `${API_URL}/sites/${site.id}/profiles`,
            {headers: getAuthHeader(eve.id)}
        )

        expect(res.status).toEqual(403)
    })

    it('gets non empty list', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})

        const site_id = site.id
        const profiles = await Profile.create([
            {id: uuid(), site_id, status: 'done'},
            {id: uuid(), site_id, status: 'done'},
            {id: uuid(), site_id, status: 'done'},
        ])

        const res = await fetch(
            `${API_URL}/sites/${site_id}/profiles`,
            {headers: getAuthHeader(user.id)}
        )
        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.length).toEqual(3)
    })
})

describe('POST /sites/:id/profiles', () => {
    it('creates with no body', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})

        const headers = Object.assign(
            { 'Content-Type': 'application/json' },
            getAuthHeader(user.id)
        )
        const method = 'POST'
        const res = await fetch(
            `${API_URL}/sites/${site.id}/profiles`,
            { method, headers }
        )
        const data = await res.json()
        expect(res.status).toEqual(200)
        expect(data).toBeDefined()
    })
})

describe('GET /profiles/:id', () => {
    it('fetches relevant model', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})
        const site_id = site.id
        const profiles = await Profile.create([
            {id: uuid(), site_id},
            {id: uuid(), site_id},
            {id: uuid(), site_id},
        ])

        const res = await fetch(
            `${API_URL}/profiles/${profiles[1].id}`,
            {headers: getAuthHeader(user.id)}
        )
        expect(res.status).toEqual(200)
        const data = await res.json()

        expect(data).toBeDefined()
        expect(data.id).toEqual(profiles[1].id)
        expect(data.results.har).toBeDefined()
        expect(data.results.lighthouse).toBeDefined()
        expect(data.results.screenshots).toBeDefined()
    })
})

describe('PATCH /profiles:id', () => {
    it('patches a profile', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})
        const profile = await Profile.create({
            id: uuid(),
            status: 'queued',
            site_id: site.id,
        })

        const headers = Object.assign(
            { 'Content-Type': 'application/json' },
            getAuthHeader(user.id)
        )
        const method = 'PATCH'
        const body = JSON.stringify({status: 'running'})
        const res = await fetch(
            `${API_URL}/profiles/${profile.id}`,
            { method, headers, body}
        )

        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.status).toEqual('running')
    })
})
