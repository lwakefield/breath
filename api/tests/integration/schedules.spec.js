const fetch = require('node-fetch')

const {
    Profile,
    Site,
    Schedule
} = require('db')

describe('GET /sites/:id/schedules', () => {
    it('gets empty list', async () => {
        const res = await fetch(`${API_URL}/sites/1/schedules`)
        expect(res.status).toEqual(200)
        expect(await res.json()).toEqual([])
    })
    it('gets non empty list', async () => {
        const site = await Site.create()
        const site_id = site.id
        const schedules = await Schedule.create([
            {site_id},
            {site_id},
            {site_id},
        ])

        const res = await fetch(`${API_URL}/sites/${site_id}/schedules`)
        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.length).toEqual(3)
    })
})

describe('GET /schedules/:id', () => {
    it('fetches relevant model', async () => {
        const site = await Site.create()
        const site_id = site.id
        const schedules = await Schedule.create([
            {site_id},
            {site_id},
            {site_id},
        ])

        const res = await fetch(`${API_URL}/schedules/${schedules[1].id}`)
        expect(res.status).toEqual(200)
        const data = await res.json()

        expect(data).toBeDefined()
        expect(data.id).toEqual(schedules[1].id)
    })
})

describe('PATCH /schedules:id', () => {
    it('patches a schedule', async () => {
        const site = await Site.create()
        const schedule = await Schedule.create({
            site_id: site.id,
            crontab: '1 * * * *'
        })

        const headers = { 'Content-Type': 'application/json' }
        const method = 'PATCH'
        const body = JSON.stringify({crontab: '2 * * * *'})
        const res = await fetch(
            `${API_URL}/schedules/${schedule.id}`,
            { method, headers, body}
        )

        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.crontab).toEqual('2 * * * *')
        expect((await Schedule.get(schedule.id)).crontab).toEqual('2 * * * *')
    })
})

describe('GET /schedules?due', () => {
    it('returns schedules that have next=null', async () => {
        const site = await Site.create()
        const schedule = await Schedule.create({site_id: site.id})

        const res = await fetch(`${API_URL}/schedules?due`)

        const data = await res.json()
        expect(data.length).toEqual(1)
        expect(data[0].id).toEqual(schedule.id)
    })
    it('returns schedules that are due', async () => {
        const site = await Site.create()
        const oneMin = 1000 * 60
        const oneMinuteAway = Date.now() - oneMin
        const schedule = await Schedule.create({
            site_id: site.id,
            next: new Date(oneMinuteAway)
        })

        const res = await fetch(`${API_URL}/schedules?due`)

        const data = await res.json()
        expect(data.length).toEqual(1)
        expect(data[0].id).toEqual(schedule.id)
    })
    it('ignores schedules that are not due', async () => {
        const site = await Site.create()
        const oneMin = 1000 * 60
        const oneMinuteAway = Date.now() + oneMin
        const schedules = await Schedule.create({
            site_id: site.id,
            next: new Date(oneMinuteAway)
        })

        const res = await fetch(`${API_URL}/schedules?due`)

        const data = await res.json()
        expect(data.length).toEqual(0)
    })
})
