/* eslint-env jest */
const fetch = require('node-fetch')
const {hashSync} = require('bcryptjs')
const omit = require('lodash/omit')

const SALT_ROUNDS = 10
const bcrypt = plaintext => hashSync(plaintext, SALT_ROUNDS)

const { User } = require('db')

describe('POST /register', () => {
    it('registers a user', async () => {
        const body = JSON.stringify({
            first_name: 'foo',
            last_name: 'bar',
            email: 'foo@bar.com',
            password: 'raboof'
        })
        const headers = { 'Content-Type': 'application/json' }
        const res = await fetch(
            `${API_URL}/register`,
            { method: 'POST', body, headers }
        )

        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data).toHaveProperty('created_at')
        expect(data).toHaveProperty('id')
        expect(omit(data, 'created_at', 'id')).toMatchSnapshot()

        // const model = await User.query().where('id', data.id)
        // expect(model).to.be.ok
    })
})

describe('POST /login', () => {
    it('returns a jwt', async () => {
        const user = await User.query().insert({
            email: 'foo@bar.com',
            password: bcrypt('raboof')
        })
        const body = JSON.stringify({
            email: 'foo@bar.com',
            password: 'raboof'
        })
        const headers = { 'Content-Type': 'application/json' }
        const res = await fetch(
            `${API_URL}/login`,
            { method: 'POST', body, headers }
        )

        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data).toHaveProperty('token')
    })

    it('fails on no user', async () => {
        const body = JSON.stringify({
            email: 'foo@bar.com',
            password: 'raboof'
        })
        const headers = { 'Content-Type': 'application/json' }
        const res = await fetch(
            `${API_URL}/login`,
            {
                method: 'POST',
                body,
                headers
            }
        )

        expect(res.status).toEqual(403)
        expect(await res.text()).toEqual('Forbidden')
    })
})

