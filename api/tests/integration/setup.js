global.API_URL = process.env.API_URL

const { knex } = require('db')

beforeEach(async () => {
  const truncate = table => {
    return knex.raw(`TRUNCATE TABLE ${table} RESTART IDENTITY CASCADE`)
  }
  await truncate('profile_metrics')
  await truncate('profiles')
  await truncate('schedules')
  await truncate('sites')
  await truncate('users')
})

afterAll(async () => {
  await knex.destroy()
})
