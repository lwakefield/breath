const jwt = require('jsonwebtoken')

const getAuthToken = userId => jwt.sign({userId}, process.env.SECRET_KEY)
const getAuthHeader = userId => {
    return { 'Authorization': `Bearer ${getAuthToken(userId)}` }
}

module.exports = getAuthHeader
