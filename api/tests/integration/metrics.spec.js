const uuid = require('uuid/v4')
const fetch = require('node-fetch')
const kue = require('kue')

const {
    ProfileMetric,
    Site,
    Profile,
    User
} = require('db')

const getAuthHeader = require('./auth')

async function makeProfile () {
    const user = await User.create()
    const site = await Site.create({user_id: user.id})
    const profile = await Profile.create({site_id: site.id, id: uuid()})

    return {user, site, profile}
}

describe('GET /profiles/:site_id/metrics', () => {
    it('gets empty list', async () => {
        const {user, profile} = await makeProfile()
        const res = await fetch(
            `${API_URL}/profiles/${profile.id}/metrics`,
            {headers: getAuthHeader(user.id)}
        )
        expect(res.status).toEqual(200)
        expect(await res.json()).toEqual([])
    })

    it('gets non empty list', async () => {
        const {user, profile} = await makeProfile()
        const profile_id = profile.id
        const metrics = await ProfileMetric.create([
            {profile_id, key: 'foo', value: 123},
            {profile_id, key: 'bar', value: 123},
            {profile_id, key: 'baz', value: 123},
        ]).returning('*')

        const res = await fetch(
            `${API_URL}/profiles/${profile.id}/metrics`,
            {headers: getAuthHeader(user.id)}
        )
        expect(res.status).toEqual(200)
        const data = await res.json()
        expect(data.length).toEqual(3)
    })
})

describe('POST /profiles/:profile_id/metrics', () => {
    it('creates a metric correctly', async () => {
        const site = await Site.create()
        const profile = await Profile.create({id: uuid(), site_id: site.id})

        const headers = { 'Content-Type': 'application/json' }
        const method = 'POST'
        const body = JSON.stringify({key: 'foo', value: 123})

        const res = await fetch(
            `${API_URL}/profiles/${profile.id}/metrics`,
            { method, headers, body }
        )
        const data = await res.json()
        expect(res.status).toEqual(200)
        expect(data).toBeDefined()
    })
})

describe('GET /sites/:site_id/metrics', () => {
    it('fetches metrics correctly', async () => {
        const user = await User.create()
        const user_id = user.id
        const [ site, otherSite ] = await Site.create([{user_id}, {user_id}])

        const profile = await Profile.create({id: uuid(), site_id: site.id})
        const profile_id = profile.id
        const metrics = await ProfileMetric.create([
            {profile_id, key: 'foo', value: 123},
            {profile_id, key: 'bar', value: 123},
            {profile_id, key: 'baz', value: 123},
        ]).returning('*')

        // The requst should ignore this metric
        const otherProfile = await Profile.create({id: uuid(), site_id: otherSite.id})
        await ProfileMetric.create({
            profile_id: otherProfile.id,
            key: 'qux',
            value: 123
        }).returning('*')

        const res = await fetch(
            `${API_URL}/sites/${site.id}/metrics`,
            {headers: getAuthHeader(user.id)}
        )
        const data = await res.json()

        expect(res.status).toEqual(200)
        expect(data).toBeDefined()
        expect(data.length).toEqual(3)
    })
})

describe('GET /sites/:site_id/metrics/:key', () => {
    it('fetches metrics correctly', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})

        const site_id = site.id
        const profiles = await Profile.create([
            {id: uuid(), site_id},
            {id: uuid(), site_id},
            {id: uuid(), site_id},
        ])
        const metrics = await ProfileMetric.create([
            {profile_id: profiles[0].id, key: 'foo', value: 123},
            {profile_id: profiles[1].id, key: 'foo', value: 234},
            {profile_id: profiles[2].id, key: 'foo', value: 345},
        ]).returning('*')

        const res = await fetch(
            `${API_URL}/sites/${site.id}/metrics/foo`,
            {headers: getAuthHeader(user.id)}
        )
        const data = await res.json()

        expect(res.status).toEqual(200)
        expect(data).toBeDefined()
        expect(data.length).toEqual(1)
    })

    it('fetches windowed metrics correctly', async () => {
        const user = await User.create()
        const site = await Site.create({user_id: user.id})

        const site_id = site.id
        const oneMin = 1000 * 60
        const now = new Date('1999')
        const metricData = [
            {value: 10, created_at: new Date(now - 15 * oneMin)},
            {value: 11, created_at: new Date(now - 30 * oneMin)},
            {value: 12, created_at: new Date(now - 45 * oneMin)},
            {value: 13, created_at: new Date(now - 60 * oneMin)},
            {value: 14, created_at: new Date(now - 75 * oneMin)},
            {value: 15, created_at: new Date(now - 90 * oneMin)},
            {value: 16, created_at: new Date(now - 105 * oneMin)},
            {value: 17, created_at: new Date(now - 120 * oneMin)},
        ]

        for (const {value, created_at} of metricData) {
            const p = await Profile.create({id: uuid(), site_id, created_at})
            const metric = await ProfileMetric.create({
                profile_id: p.id, key: 'foo', value
            }).returning('*')
        }

        const res = await fetch(
            `${API_URL}/sites/${site.id}/metrics/foo?to=${now.toISOString()}`,
            {headers: getAuthHeader(user.id)}
        )
        const data = await res.json()

        expect(res.status).toEqual(200)
        expect(data.length).toEqual(2)
        expect(data[0].value).toEqual(11.5)
        expect(data[1].value).toEqual(15.5)
    })
})
