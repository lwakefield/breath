from node:7.8.0-alpine

workdir /app
copy package.json /app/
run npm install
copy . /app/

cmd scripts/wait-for.sh db:5432 && \
  node_modules/.bin/knex migrate:latest && \
  node_modules/.bin/babel-node src/index.js
