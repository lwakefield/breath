// Update with your config settings.

module.exports = {
  client: 'postgresql',
  connection: process.env.DATABASE_URI,
  migrations: {
    directory: 'src/db/migrations'
  }
};
