const {hashSync, compareSync} = require('bcryptjs')

const SALT_ROUNDS = 10
const bcrypt = plaintext => hashSync(plaintext, SALT_ROUNDS)

exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  await knex('schedules').del()
  await knex('sites').del()
  await knex('users').del()

  const [ user ] = await knex('users')
    .insert({id: 1, email: 'test@test.com', password: bcrypt('password123')})
    .returning('*')

  const [ site ] = await knex('sites')
    .insert({url: 'https://news.ycombinator.com', user_id: user.id})
    .returning('*')

  await knex('schedules')
    .insert({crontab: '*/5 * * * *', site_id: site.id})
};
