const path = require('path')
const fs = require('fs')

const nodeModules = {}
fs.readdirSync('node_modules')
    .filter(v => v !== '.bin')
    .forEach(mod => {
        nodeModules[mod] = 'commonjs ' + mod
    })

module.exports = {
    entry: './src/index.js',
    target: 'node',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js'
    },
    externals: nodeModules,
    module: {
        rules: [
            {
                test: /\.js$/,
                include: /src/,
                loader: 'babel-loader'
            }
        ]
    },
    node: {
        fs: 'empty',
        net: 'empty',
        module: 'empty',
        tls: 'empty'
    },
    resolve: {
        modules: [path.resolve(__dirname, "src"), 'node_modules']
    }
}
