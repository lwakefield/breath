const keys = [
    'DOESNT_EXIST',
    'FORBIDDEN',
]

const errors = keys.reduce(
    (acc, curr) => Object.assign(acc, {[curr]: curr}),
    {}
)
module.exports = errors
