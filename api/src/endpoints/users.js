const {hashSync, compareSync} = require('bcryptjs')
const jwt = require('jsonwebtoken')
const omit = require('lodash/omit')

const {User} = require('db')
const { register } = require('decorators')
const config = require('config')

const SALT_ROUNDS = 10
const bcrypt = plaintext => hashSync(plaintext, SALT_ROUNDS)

@register('/register')
class Register {
    async post (req, res) {
        const {
            first_name = '',
            last_name = '',
            email = '',
        } = req.body
        const plainTextPassword = req.body.password || ''
        const password = bcrypt(plainTextPassword)
        const data = {first_name, last_name, email, password}
        const user = await User.create(data)

        const response = omit(user.toJSON(), ['password'])
        res.json(response)
    }
}

@register('/login')
class Login {
    async post (req, res) {
        const {
            email = '',
            password = ''
        } = req.body
        const users = await User.where('email', email)
        const user = users[0]

        if (!user) {
            return res.sendStatus(403)
        }
        if (!compareSync(password, user.password)) {
            return res.sendStatus(403)
        }

        const userId = user.id
        const token = jwt.sign({userId}, config.SECRET_KEY)
        res.json({token})
    }
}

module.exports = { Register, Login }
