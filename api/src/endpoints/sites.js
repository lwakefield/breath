const {Site, Schedule, User} = require('db')
const {
    register,
    paginated,
    authenticated,
    internal,
    uses,
} = require('decorators')

@register('/sites')
class Sites {
    @paginated
    @internal
    async get (req, res) {
        const {offset, limit} = req.query

        const sites = await Site.query().offset(offset).limit(limit)
        res.json(sites.map(v => v.toJSON()))
    }
}

@register('/users/:user_id/sites')
class UserSites {
    @paginated
    @authenticated
    @uses(User, 'user_id', {owns: true})
    async get (req, res) {
        const {user_id} = req.params
        const {offset, limit} = req.query

        const sites = await Site.where('user_id', user_id)
            .offset(offset)
            .limit(limit)

        res.json(sites.map(v => v.toJSON()))
    }

    @authenticated
    @uses(User, 'user_id', {owns: true})
    async post (req, res) {
        const {user_id} = req.params

        const {url} = req.body
        const data = {url, user_id: req.user.id}
        const site = await Site.create(data)

        const crontab = '*/5 * * * *'
        const site_id = site.id
        const schedule = await Schedule.create({ site_id, crontab })

        res.json(site.toJSON())
    }
}

@register('/sites/:site_id')
class SingleSite {
    @authenticated
    @uses(Site, 'site_id', {owns: true})
    async get (req, res) {
        res.json(req.site.toJSON())
    }
}

module.exports = {
    Sites,
    Site: SingleSite,
    UserSites
}
