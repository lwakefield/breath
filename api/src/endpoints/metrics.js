const uuid = require('uuid')
const { raw } = require('knex')
const toPairs = require('lodash/toPairs')
const moment = require('moment')

const {ProfileMetric, Profile, Site} = require('db')
const {
    register,
    paginated,
    authenticated,
    internal,
    uses,
} = require('decorators')

@register('/profiles/:profile_id/metrics')
class ProfileMetrics {
    @paginated
    @authenticated
    @uses(Profile, 'profile_id', {owns: true})
    async get (req, res) {
        const { profile_id } = req.params
        const {offset, limit} = req.query

        const metrics = await ProfileMetric
            .where('profile_id', profile_id)
            .offset(offset)
            .limit(limit)

        res.json(metrics.map(v => v.toJSON()))
    }

    @internal
    async post (req, res) {
        const {key, value} = req.body
        const profile_id = req.params.profile_id

        const metric = await ProfileMetric.create({profile_id, key, value})
            .returning('*')
        res.json(metric.toJSON())
    }

    @internal
    async patch (req, res) {
        const profile_id = req.params.profile_id
        const insertProfileId = v => Object.assign(v, {profile_id})
        const metricData = toPairs(req.body).map(([key, value]) => ({key, value, profile_id}))

        const metrics = await ProfileMetric.create(metricData)
            .returning('*')
        res.json(metrics.map(v => v.toJSON()))
    }
}

@register('/sites/:site_id/metrics')
class SiteMetrics {
    @authenticated
    @paginated
    @uses(Site, 'site_id', {owns: true})
    async get (req, res) {
        const { site_id } = req.params
        const {offset, limit} = req.query

        const metrics = await ProfileMetric.query()
            .joinRelation('profile', {alias: 'p'})
            .where('p.site_id', site_id)
            .offset(offset)
            .limit(limit)

        res.json(metrics.map(v => v.toJSON()))
    }
}

@register('/sites/:site_id/metrics/:key')
class SiteSingleMetric {
    @paginated
    @authenticated
    @uses(Site, 'site_id', {owns: true})
    async get (req, res) {
        const { site_id, key } = req.params
        let { from, to } = req.query
        if (!to && !from) {
            to = moment()
            from = to.clone().subtract(7, 'days')
        } else if (!from) {
            from = moment(to).subtract(7, 'days')
        } else if (!to) {
            to = moment(from).add(7, 'days')
        }

        from = moment(from).toISOString()
        to = moment(to).toISOString()

        const metrics = await ProfileMetric.query()
            .joinRelation('profile', {alias: 'p'})
            .where('p.site_id', site_id)
            .whereBetween('p.created_at', [from, to])
            .andWhere('key', key)
            .select(raw("date_trunc('hour', p.created_at) as timestamp"))
            .select(raw("avg(value) as value"))
            .orderBy('timestamp', 'desc')
            .groupBy('timestamp')
        res.json(metrics.map(v => v.toJSON()))
    }
}

module.exports = {
    ProfileMetrics,
    SiteMetrics,
    SiteSingleMetric
}
