const {Schedule} = require('db')
const {
    register,
    paginated,
    internal,
    uses
} = require('decorators')

// TODO: internal
@register('/schedules')
class Schedules {
    @internal
    @paginated
    async get (req, res) {
        const {due} = req.query

        if (due === undefined) {
            res.json([])
            return
        }

        const dueSchedules = await Schedule
            .where('next', '<', new Date())
            .orWhere('next', null)

        res.json(dueSchedules.map(v => v.toJSON()))
    }
}

// TODO: protect
@register('/schedules/:schedule_id')
class SingleSchedule {
    @internal
    @uses(Schedule, 'schedule_id')
    async patch (req, res) {
        await req.schedule.update(req.body)
        res.json(req.schedule.toJSON())
    }

    @internal
    @uses(Schedule, 'schedule_id')
    async get (req, res) {
        res.json(req.schedule.toJSON())
    }
}

// TODO: protext
@register('/sites/:site_id/schedules')
class SiteSchedules {
    @internal
    async get (req, res) {
        const schedules = await Schedule.where('site_id', req.params.site_id)
        res.json(schedules.map(v => v.toJSON()))
    }
}

module.exports = { Schedules, SingleSchedule, SiteSchedules }
