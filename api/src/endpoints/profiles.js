const uuid = require('uuid/v4')
const AWS = require('aws-sdk')
const set = require('lodash/set')

const config = require('config')
const {Profile, Site} = require('db')
const {
    register,
    paginated,
    authenticated,
    internal,
    uses
} = require('decorators')

const s3 = new AWS.S3(config.AWS_CONFIG)

@register('/sites/:site_id/profiles')
class Profiles {
    @paginated
    @authenticated
    @uses(Site, 'site_id', {owns: true})
    async get (req, res) {
        const {offset, limit, metrics = ''} = req.query
        const {site_id} = req.params

        const profiles = await Profile.where('site_id', site_id)
            .where('status', 'done')
            .offset(offset)
            .limit(limit)
            .eager('metrics')
            .modifyEager('metrics', builder =>
                builder.whereIn('key', metrics.split(','))
            )
            .orderBy('created_at', 'desc')
            .map(v => {
                const metrics = v.metrics || {}
                v.metrics = metrics.reduce(
                    (acc, v) => Object.assign(acc, {[v.key]: v.value}),
                    {}
                )
                return v
            })

        // TODO: add x-total header?
        res.json(profiles.map(v => v.toJSON()))
    }

    @internal
    @uses(Site, 'site_id')
    async post (req, res)  {
        const {site_id} = req.params

        const {url} = req.site
        const profile = await Profile.create({id: uuid(), site_id})

        const data = profile.toJSON()
        // TODO: The workere should fetch the url
        data.url = url

        res.json(data)
    }
}

@register('/profiles/:profile_id')
class SingleProfile {

    @authenticated
    @uses(Profile, 'profile_id', {owns: true})
    async get (req, res) {
        const response = req.profile.toJSON()
        const addResult = (path, Key) => {
            const ONE_WEEK = 7 * 24 * 60 * 60
            const params = {Bucket: config.S3_BUCKET, Key, Expires: ONE_WEEK}
            set(response, path, s3.getSignedUrl('getObject', params))
        }
        addResult('results.har', `${response.id}.har`)
        addResult('results.lighthouse', `${response.id}.json`)
        addResult('results.screenshots', `${response.id}-0.screenshots.json`)

        res.json(response)
    }

    @internal
    @uses(Profile, 'profile_id')
    async patch (req, res)  {
        await req.profile.update(req.body)
        res.json(req.profile.toJSON())
    }
}

module.exports = {Profiles, Profile: SingleProfile}
