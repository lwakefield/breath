function uses (Model, paramName, options) {
    const { owns = false } = options || {}

    return function (target, name, descriptor) {
        const fn = descriptor.value

        descriptor.value = async (req, res, ...rest) => {
            const model = await Model.get(req.params[paramName])
            if (!model) {
                return res.sendStatus(404)
            }

            if (owns) {
                const isOwner = await req.authed.isOwner(model)
                if (isOwner === false) {
                    return res.sendStatus(403)
                }
            }

            req[Model.name.toLowerCase()] = model
            return fn(req, res, ...rest)
        }

        return descriptor
    }
}

module.exports = uses
