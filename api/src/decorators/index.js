const paginated = require('./paginated')
const authenticated = require('./authenticated')
const register = require('./register')
const internal = require('./internal')
const uses = require('./uses')

module.exports = {
    paginated,
    authenticated,
    register,
    internal,
    uses,
}
