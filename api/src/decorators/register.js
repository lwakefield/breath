function register (endpoint) {
    return (target, name, descriptor) => {
        target.endpoint = endpoint

        return descriptor
    }
}

module.exports = register
