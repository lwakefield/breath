function paginated (target, name, descriptor) {
    const fn = descriptor.value
    descriptor.value = (req, ...rest) => {
        const offset = Number(req.query.offset || 0)
        const limit = Math.min(Number(req.query.limit || 20), 100)

        req.query.offset = offset
        req.query.limit = limit

        return fn(req, ...rest)
    }

    return descriptor
}

module.exports = paginated
