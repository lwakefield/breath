function internal (target, name, descriptor) {
    const fn = descriptor.value
    descriptor.value = (req, res, ...rest) => {
        if (req.headers['x-real-ip']) {
            return res.sendStatus(401)
        }

        return fn(req, res, ...rest)
    }

    return descriptor
}

module.exports = internal
