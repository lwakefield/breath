const jwt = require('jsonwebtoken')
const { User } = require('db')
const config = require('config')

function authenticated (target, name, descriptor) {
    const fn = descriptor.value
    descriptor.value = async (req, res, ...rest) => {

        try {
            const token = req.get('Authorization').replace('Bearer ', '')
            const decoded = jwt.verify(token, config.SECRET_KEY);
            req.authed = await User.get(decoded.userId)
            if (req.authed === undefined) {
                return res.sendStatus(401)
            }
        } catch (e) {
            return res.sendStatus(401)
        }

        return fn(req, res, ...rest)
    }

    return descriptor
}

module.exports = authenticated
