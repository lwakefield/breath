const { 
    Model: _Model,
    QueryBuilder: _QueryBuilder,
    QueryBuilderBase
} = require('objection')

function hasMany (owner, related) {
    // We assume to to be pluralized (ends with s)
    const singularOwner = owner.tableName.replace(/s$/, '')

    return relation(
        Model.HasManyRelation,
        related,
        `${owner.tableName}.id`,
        `${related.tableName}.${singularOwner}_id`
    )
}

function belongsTo (owner, related) {
    // We assume to to be pluralized (ends with s)
    const singularRelated = related.tableName.replace(/s$/, '')

    return relation(
        Model.BelongsToOneRelation,
        related,
        `${owner.tableName}.${singularRelated}_id`,
        `${related.tableName}.id`
    )
}

function relation (relation, modelClass, from, to) {
    return { relation, modelClass, join: { from, to } }
}

class QueryBuilder extends _QueryBuilder {
    get (id) {
        return this.where('id', id).first()
    }
    create (data = {}) {
        return this.insert(data)
    }
    where (...args) {
        return super.where(...args)
    }
}

class Model extends _Model {
    static get QueryBuilder() { return QueryBuilder }

    $beforeInsert () {
        if (!this.created_at) {
            this.created_at = new Date()
        }
    }
    $beforeUpdate () {
        this.updated_at = new Date()
    }
    update (data) {
        return this.$query().update(data)
    }

    static __initRelationsMappings () {
        if (!this.relationMappings) {
            this.relationMappings = {}
        }
    }
    static hasMany (name, model) {
        this.__initRelationsMappings()
        this.relationMappings[name] = hasMany(this, model)
    }
    static belongsTo (name, model) {
        this.__initRelationsMappings()
        this.relationMappings[name] = belongsTo(this, model)
    }
}

const fns = Object.getOwnPropertyDescriptors(QueryBuilder.prototype)
Object.keys(fns).filter(v => v !== 'constructor')
    .forEach(name => {
        Model[name] = function (...args) {
            return this.query()[name](...args)
        }
    })

module.exports = Model
