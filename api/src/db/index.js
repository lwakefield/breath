const Knex = require('knex')
const Model = require('./model')
const config = require('config')

const knex = Knex(config.DATABASE_URI)
Model.knex(knex)

class User extends Model {
    static get tableName () {
        return 'users'
    }
    async isOwner (model) {
        const owner = await model.owner()

        return owner.id === this.id
    }
    async owner () {
        return this
    }
}

class Site extends Model {
    static get tableName () {
        return 'sites'
    }
    async owner () {
        const site = await this.$clone().$loadRelated('user')
        return site.user
    }
}

class Schedule extends Model {
    static get tableName () {
        return 'schedules'
    }
    async owner () {
        const schedule = await this.$clone().$loadRelated('site.user')
        return schedule.site.user
    }
    async ownedBy () {
        const users = await User.query()
            .leftJoin('sites', 'sites.user_id', 'users.id')
            .leftJoin('schedules', 'schedules.site_id', 'sites.id')
            .where('schedules.id', this.id)
            .limit(1)
        return users.length === 0 ? null : users[0]
    }
    get owner () {
        return this.site.user
    }
}

class Profile extends Model {
    static get tableName () {
        return 'profiles'
    }
    async owner () {
        const profile = await this.$clone().$loadRelated('site.user')
        return profile.site.user
    }
}

class ProfileMetric extends Model {
    static get tableName () {
        return 'profile_metrics'
    }
    async owner () {
        const profile = await this.$clone().$loadRelated('profile.site.user')
        return profile.site.user
    }
}

User.hasMany('sites', Site)
Site.hasMany('schedules', Schedule)
Site.hasMany('profiles', Profile)
Profile.hasMany('metrics', ProfileMetric)

Site.belongsTo('user', User)
Profile.belongsTo('site', Site)
ProfileMetric.belongsTo('profile', Profile)

module.exports = {
    User,
    Site,
    Schedule,
    Profile,
    ProfileMetric,

    knex
}
