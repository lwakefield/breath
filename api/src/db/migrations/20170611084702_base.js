async function up (knex) {
  await knex.schema.createTable('users', table => {
    table.increments('id')
    table.timestamps()
    table.string('email').unique()
    table.string('first_name')
    table.string('last_name')
    table.string('password')
  })
  await knex.schema.createTable('sites', table => {
    table.increments('id')
    table.timestamps()
    table.string('url')
    table.integer('user_id')
    table.foreign('user_id').references('users.id')
  })
  await knex.schema.createTable('schedules', table => {
    table.increments('id')
    table.timestamps()
    table.integer('site_id')
    table.foreign('site_id').references('sites.id')
    table.string('crontab')
    table.dateTime('next')
  })
  await knex.schema.createTable('profiles', table => {
    table.uuid('id').primary()
    table.timestamps()
    table.integer('site_id')
    table.foreign('site_id').references('sites.id')
    table.string('status')
  })
  await knex.schema.createTable('profile_metrics', table => {
    table.timestamps()
    table.uuid('profile_id')
    table.foreign('profile_id').references('profiles.id')
    table.string('key')
    table.primary(['profile_id', 'key'])
    table.float('value')
  })
}

async function down (knex) {
  await knex.schema.dropTableIfExists('profile_metrics')
  await knex.schema.dropTableIfExists('profiles')
  await knex.schema.dropTableIfExists('schedules')
  await knex.schema.dropTableIfExists('sites')
  await knex.schema.dropTableIfExists('users')
}

module.exports = {up, down}
