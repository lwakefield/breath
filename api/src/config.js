const fs = require('fs')

function getSecret (name) {
    const path = `/run/secrets/${name}`
    if (!fs.existsSync(path)) {
        return process.env[name] || null
    }

    const buffer = fs.readFileSync(path)
    return buffer.toString().trim()
}

module.exports = {
    SECRET_KEY: getSecret('SECRET_KEY'),
    AWS_CONFIG: {
        accessKeyId:      getSecret('AWS_ACCESS_KEY_ID'),
        secretAccessKey:  getSecret('AWS_SECRET_ACCESS_KEY'),
        endpoint:         process.env.AWS_ENDPOINT,
        s3ForcePathStyle: process.env.AWS_S3_FORCE_PATH_STYLE || false
    },
    S3_BUCKET: process.env.S3_BUCKET,
    PORT: process.env.PORT || 80,
    DATABASE_URI: process.env.DATABASE_URI,
}
