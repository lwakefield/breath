const path = require('path')
process.env.NODE_PATH = path.join(__dirname, '.')
require('module').Module._initPaths()

const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')

const config = require('config')

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use((req, res, next) => {
    const headers = [
        ['Access-Control-Allow-Origin', '*'],
        ['Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept']
    ]
    headers.forEach(([k, v]) => res.header(k, v))
    next();
});

const verbs = [
    'delete',
    'get',
    'head',
    'options',
    'patch',
    'post',
    'purge',
    'put'
]

function registerAll (routeMap) {
    Object.keys(routeMap)
        .map(k => routeMap[k])
        .forEach(v => register(v))
}

function register (RouteClass) {
    const instance = new RouteClass()
    const {endpoint} = RouteClass

    for (const verb of verbs) {
        if (instance[verb]) {
            app[verb](endpoint, wrap(instance[verb]))
        }
    }
}

function wrap (fn) {
    return function (req, res, next) {
        try {
            const result = fn(req, res, next)
            const isPromise = result instanceof Promise
            if (isPromise === true) {
                result.catch(next)
            }
        } catch (e) {
            next(e)
        }
    }
}

registerAll(require('endpoints/profiles'))
registerAll(require('endpoints/users'))
registerAll(require('endpoints/sites'))
registerAll(require('endpoints/schedules'))
registerAll(require('endpoints/metrics'))

const server = app.listen(
    config.PORT,
    () => console.log(`Listening on port ${config.PORT}`)
)

module.exports = { server }
