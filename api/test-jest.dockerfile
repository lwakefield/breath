from node:7.8.0-alpine

workdir /app
copy package.json /app/
run npm install
copy . /app/
run node_modules/.bin/webpack

cmd scripts/wait-for.sh --timeout=60 api:80 && \
    npm run test-integration
