const fetch = require('node-fetch')
const {parseExpression: crontabParse} = require('cron-parser')

const POLL_RATE = 10 * 1000
const LIGHTHOUSE_LAMBDA_ENDPOINT = process.env.LIGHTHOUSE_LAMBDA_ENDPOINT


const getOptionsForMethod = method => ({method, headers: {'Content-Type': 'application/json'}})
const post = (url, options = {}) => fetch(
    url,
    Object.assign({}, getOptionsForMethod('POST'), options)
).then(v => v.json())

const patch = (url, options = {}) => {
    return fetch(
    url,
    Object.assign({}, getOptionsForMethod('PATCH'), options)
).then(v => v.json()) }

async function run () {
    const response = await fetch('http://api/schedules?due')
    console.log('Checking for due schedules');
    const schedules = await response.json()
    schedules.length && console.log(`Processing ${schedules.length} due schedules`)

    for (const schedule of schedules) {
        runLighthouse(schedule)
    }
}

async function runLighthouse (schedule) {
    // Schedule the next run
    const next = crontabParse(schedule.crontab).next()
    const res = await patch(`http://api/schedules/${schedule.id}`, {
        body: JSON.stringify({next}),
    })

    // Create a new profile
    const profile = await post(`http://api/sites/${schedule.site_id}/profiles`)

    // Run lighthouse
    const metrics = await post(
        LIGHTHOUSE_LAMBDA_ENDPOINT,
        {
            body: JSON.stringify({id: profile.id, url: profile.url})
        }
    )

    // Save the metrics
    await Promise.all([
        // Should this be a put?
        patch(
            `http://api/profiles/${profile.id}/metrics`,
            {
                body: JSON.stringify(metrics)
            }
        ),
        patch(
            `http://api/profiles/${profile.id}`,
            {body: JSON.stringify({'status': 'done'})}
        )
    ])
}

console.log('Starting scheduler')
run()
setInterval(run, POLL_RATE)
