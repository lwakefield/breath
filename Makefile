.PHONY: install deploy

install:
	rm -rf **/node_modules
	npm install
	(cd api        && npm install)
	(cd kue-ui     && npm install)
	(cd queue      && npm install)
	(cd frontend   && npm install)
	(cd scheduler  && npm install)

staging-up:
	export $(shell cat staging.env | xargs); \
	aws cloudformation create-stack \
		--stack-name breath-staging \
		--template-url "$${TEMPLATE_URL}" \
		--capabilities CAPABILITY_IAM \
		--parameters \
			ParameterKey=KeyName,ParameterValue="$${KEY_NAME}" \
			ParameterKey=DBName,ParameterValue="$${DB_NAME}" \
			ParameterKey=DBUser,ParameterValue="$${DB_USER}" \
			ParameterKey=DBPassword,ParameterValue="$${DB_PASSWORD}" \
			ParameterKey=ManagerSize,ParameterValue=1 \
			ParameterKey=ClusterSize,ParameterValue=1

staging-down:
	aws cloudformation delete-stack --stack-name breath-staging

serve:
	docker-compose -f docker-compose.dev.yml up --build

serve-test-db:
	docker run --rm \
		--name breathtestdb \
		-e POSTGRES_DB=breathtestdb \
		-p 5432:5432 \
		--shm-size=1g \
		labianchin/docker-postgres-for-testing

kill-test-db:
	docker stop breathtestdb

deploy:
	sh scripts/deploy.sh
