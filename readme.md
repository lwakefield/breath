# Install

sudo echo "
127.0.0.1       breath.dev
127.0.0.1       api.breath.dev
127.0.0.1       app.breath.dev
127.0.0.1       kue-ui.breath.dev" >> /etc/hosts

# DB migrations

`sequelize db:migrate --url mysql://root:mysecretpassword@localhost:3306/breath`
