const fs = require('fs')
const devcert = require('devcert-san').default

devcert('breath', {installCertutil: true})
    .then(ssl => {
        const {key, cert} = ssl
        fs.mkdirSync('certs')
        fs.writeFileSync('certs/breath.dev.crt', cert)
        fs.writeFileSync('certs/breath.dev.key', key)
    })
