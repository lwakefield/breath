#!/usr/bin/env node
const {execSync} = require('child_process')

const isCI = process.env.CI !== undefined
const branch = isCI
    ? process.env.CI_COMMIT_REF_NAME
    : execSync('git rev-parse --abbrev-ref HEAD').toString()
const hash = isCI
    ? process.env.CI_COMMIT_SHA
    : execSync('git rev-parse HEAD').toString()

console.log(`${branch.trim()}-${hash.trim().slice(0, 5)}`)
