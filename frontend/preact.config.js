import webpack from 'webpack'
import { resolve } from 'path'

export default function (config, env, helpers) {
    config.plugins.push(new webpack.DefinePlugin({
        API_URL: JSON.stringify(process.env.API_URL),
        RESULTS_URL: JSON.stringify(process.env.RESULTS_URL)
    }))
    config.resolve.modules.push(resolve(__dirname, 'src'))
}
