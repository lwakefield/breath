import Stores from 'zay/stores'

import Api from 'lib/api'

export function register (data) {
    return Api.post('register', data).then(v => {
        const {email, password} = data
        return login({email, password})
    })
}

export function login (data) {
    return Api.post('login', data).then(v => {
        localStorage.setItem('authToken', v.token)
        Stores.mutate('auth/token', v.token)
    })
}

export function getJWT () {
    const token = Stores.get('auth/token')

    if (!token) {
        return null
    }

    const [ header, payload, signature ] = token.split('.')

    return {
        header: JSON.parse(atob(header)),
        payload: JSON.parse(atob(payload)),
        signature: signature,
    }
}

export function getClaims () {
    const jwt = getJWT()
    if (!jwt) return null

    return jwt.payload
}

export function getUserId () {
    return (getClaims() || {}).userId || null
}

export function logout () {
    localStorage.removeItem('authToken')
    Stores.mutate('auth/token', undefined)
}
