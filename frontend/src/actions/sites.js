import Stores from 'zay/stores'

import { getUserId } from 'actions/auth'
import Api from 'lib/api'

export function loadSites () {
    return Api.get('users', getUserId(), 'sites')
        .then(response => response.forEach(v =>
            Stores.mutate(`sites/${v.id}`, v)
        ))
}

export function loadSite (id) {
    return Api.get('sites', id)
        .then(v => Stores.mutate(`sites/${id}`, v))
}

export function addSite (url) {
    const path = ['users', getUserId(), 'sites']
    return Api.post(...path, {url})
        .then(v => Stores.mutate(`sites/${v.id}`, v))
}
