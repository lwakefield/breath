import Stores from 'zay/stores'
import sortBy from 'lodash/sortBy'
import merge from 'lodash/merge'
import get from 'lodash/get'

import { getUserId } from 'actions/auth'
import Api from 'lib/api'

export function loadMetrics (siteId, metric, _query) {
    const path = ['sites', siteId, 'metrics', metric]
    const window = 'hour'
    const query = Object.assign({ window }, _query)

    return Api.get(...path, query)
        .then(result => {
            Stores.mutate(`siteMetrics/${siteId}.${metric}`, result)
        })
}

export function _loadProfiles (profiles) {
    const siteId = get(profiles, '0.site_id')
    const allProfiles = Stores.get(`siteProfiles/${siteId}`, [])
    // merge any existing profiles and push any new profiles
    for (const profile of profiles) {
        const existingProfileIndex = allProfiles.findIndex(v => v.id === profile.id)
        if (existingProfileIndex !== -1) {
            const existingProfile = allProfiles[existingProfileIndex]
            const merged = merge(existingProfile, profile)
            allProfiles[existingProfileIndex] = merged
        } else {
            allProfiles.push(profile)
        }
    }
    const sorted = sortBy(
        allProfiles,
        v => new Date(v.created_at)
    ).reverse()

    const linked = sorted.map((v, k, a) => {
        const prev = a[k - 1]
        const next = a[k + 1]
        if (prev) v.prevId = prev.id
        if (next) v.nextId = next.id
        return v
    })

    Stores.mutate(`siteProfiles/${siteId}`, linked)
    linked.forEach(v => Stores.mutate(`profiles/${v.id}`, v))

    return linked
}

