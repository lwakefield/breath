export function formatMillis (ms) {
    return (ms / 1000).toFixed(2) + 's'
}
