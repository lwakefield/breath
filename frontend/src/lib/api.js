import entries from 'lodash/entries'

function doWithNoBody (verb, ...path) {
    return doWithBody(verb, ...path, undefined)
}

function doWithBody (verb, ...args) {
    const [path, data] = argsToPathAndData(args)
    const filteredPath = path.filter(Boolean)

    const maybeQuery = filteredPath[filteredPath.length - 1]
    const hasQuery = typeof maybeQuery === 'object' 
    let queryString = ''
    if (hasQuery) {
        queryString = entries(maybeQuery)
            .map(v => v.join('='))
            .join('&')
    }

    const uri = hasQuery
        ? [API_URL, ...filteredPath.slice(0, -1)].join('/')
            .concat('?', queryString)
        : [API_URL, ...filteredPath].join('/')

    const headers = {
        'Content-Type': 'application/json'
    }
    if (localStorage.authToken) {
        headers['Authorization'] = `Bearer ${localStorage.authToken}`
    }

    return fetch(
        uri,
        {
            method: verb.toUpperCase(),
            headers,
            body: JSON.stringify(data)
        }
    ).then(response => {
        if (!String(response.status).match(/20\d/)) {
            throw new FetchError(response)
        }
        return response.json()
    })
}

function argsToPathAndData (args) {
    const path = args.slice(0, args.length - 1)
    const data = args[args.length - 1]
    return [path, data]
}

class FetchError extends Error {
    constructor (response) {
        super()
        this.message = `${response.url} received: ${response.status}`
        this.response = response 
    }
}

function doFetch (uri, options) {
    return fetch(
        uri,
        {
            method: verb.toUpperCase(),
            headers,
            body: JSON.stringify(data)
        }
    ).then(response => {
        if (!String(response.status).match(/20\d/)) {
            throw new FetchError(response)
        }
        return response.json()
    })
}

export default {
    post:   (...args) => doWithBody('POST', ...args),
    put:    (...args) => doWithBody('PUT', ...args),
    patch:  (...args) => doWithBody('PATCH', ...args),

    get:    (...args) => doWithNoBody('GET', ...args),
    delete: (...args) => doWithNoBody('DELETE', ...args),
}
