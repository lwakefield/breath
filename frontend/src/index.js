import 'url-polyfill'
import Stores from 'zay/stores'

import App from './components/app'
import './style'

Stores.register('sites')
Stores.register('siteProfiles')
Stores.register('siteMetrics')
Stores.register('profiles')
Stores.register('profileHars')
Stores.register('profiles')
const isBrowser = typeof window !== "undefined"
Stores.register('auth', {token: isBrowser && localStorage.authToken})

if (module.hot) {
    window.Stores = Stores
}

export default App;
