import { h, Component } from 'preact'
import give from 'zay/give'
import toPairs from 'lodash/toPairs'
import get from 'lodash/get'
import values from 'lodash/values'
import sum from 'lodash/sum'
import Stores from 'zay/stores'
import dateFormat from 'dateformat'
import { fromHar } from 'perf-cascade/dist/perf-cascade.js'
import 'perf-cascade/dist/perf-cascade.css'
import { bind } from 'decko'

import { loadHar, loadProfile, loadProfiles } from 'actions/profile'
import { loadSite } from 'actions/sites'
import ProfileOverview from 'components/profile-overview'
import styles from './styles'

@give(props => ({
    profile: `profiles/${props.id}`,
    har: `profileHars/${props.id}`
}))
export default class Profile extends Component {
    refs = {}

    componentDidMount () {
        loadProfile(this.props.id)
            .then(v => Promise.all([
                loadHar(this.props.id),
                loadSite(v.site_id)
            ]))
    }

    componentDidUpdate () {
        if (this.refs.waterfall && this.props.har) {
            const svg = fromHar(this.props.har, {showMimeTypeIcon: false})
            this.refs.waterfall.appendChild(svg)
        }
    }

    @bind
    mountWaterfall (ref) {
        this.refs.waterfall = ref
    }

    render ({profile}) {
        if (!profile) return null

        return (
            <div class={styles.profile}>
                <h2>
                    <SiteUrl siteId={profile.site_id} />
                    &nbsp;-&nbsp;
                    { dateFormat(profile.created_at, 'd mmm yy h:MM TT') }
                </h2>

                <div class={styles.metricVideoWrapper}>
                    <ProfileOverview id={this.props.id} />

                    <video
                        src={`${RESULTS_URL}/${this.props.id}/video/0.mp4`}
                        autoplay
                        onClick={e => e.target.setAttribute('controls', true)}
                    />
                </div>
                <div ref={this.mountWaterfall}/>
            </div>
        )
    }
}

@give(p => ({url: `sites/${p.siteId}.url`}))
class SiteUrl extends Component {
    render ({url}) {
        if (!url) return null
        const {hostname} = new URL(url)
        return hostname
    }
}
