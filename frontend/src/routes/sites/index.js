import { h, Component } from 'preact'
import { Link } from 'preact-router'
import give from 'zay/give'
import { bind } from 'decko'
import get from 'lodash/get'

import { loadSites, addSite } from 'actions/sites'
import { loadProfiles } from 'actions/profile'
import styles from './styles'

@give(() => ({'sites': 'sites'}))
export default class Sites extends Component {
    componentDidMount () {
        loadSites()
    }

    render ({sites}) {
        return (
            <div>
                <h2>Sites Monitored</h2>
                {Object.keys(sites).map(k =>
                    <SiteListItem key={k} id={k} />
                )}
                <NewSiteForm />
            </div>
        )
        return <h1>hello world from Sites</h1>
    }
}

@give(p => ({
    'site': `sites/${p.id}`,
    'lastProfile': `siteProfiles/${p.id}.0`
}))
class SiteListItem extends Component {
    componentDidMount () {
        loadProfiles(this.props.id, {metrics: 'lighthouse-score,lighthouse-speed-index-metric', limit: 1})
    }
    render ({site, lastProfile}) {
        const { hostname } = new URL(site.url)
        const href = `/sites/${site.id}`
        const time = (
            get(lastProfile, 'metrics.fullyLoaded', 0) / 1000 
        ).toFixed(2) + ' s'
        console.log(lastProfile);
        return (
            <Link href={href} class={styles.siteListItem}>
                <h3>{hostname}</h3>
                <span>
                    {get(lastProfile, 'metrics.lighthouse-score', 0).toFixed(2)}
                </span>
            </Link>
        )
    }
}

class NewSiteForm extends Component {
    refs = {}

    @bind
    mountRef (v) {
        if (v) {
            this.refs[v.getAttribute('name')] = v
        }
    }

    @bind
    addSite (e) {
        e.preventDefault()
        e.stopPropagation()
        addSite(this.refs.url.value)
    }

    render () {
        return (
            <form class={styles.newSiteForm} onSubmit={this.addSite}>
                <input
                    type="url"
                    name="url"
                    placeholder="https://wikipedia.org"
                    ref={this.mountRef}
                />
                <button>Add Site</button>
            </form>
        )
    }
}
