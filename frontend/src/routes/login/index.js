import { h, Component } from 'preact';
import { route, Link } from 'preact-router'
import { bind } from 'decko'

import { login } from 'actions/auth'

import style from './style';

export default class Login extends Component {
    refs = {}

    @bind
    mountRef (name) {
        return c => this.refs[name] = c
    }

    @bind
    tryLogin (e) {
        e.preventDefault()
        e.stopPropagation()
        login({
            email: this.refs.email.value,
            password: this.refs.password.value,
        }).then(v => route('/sites'))
    }

    render() {
        return (
            <div class={style.login}>
                <form onSubmit={this.tryLogin}>
                    <h2>Login</h2>
                    <div class={style.fields}>

                        <input
                            type="email"
                            placeholder="email"
                            ref={this.mountRef('email')}
                        />
                        <input
                            type="password"
                            placeholder="password"
                            ref={this.mountRef('password')}
                        />
                        <button>Login</button>

                        <Link href="/register">Or register here</Link>
                    </div>
                </form>
            </div>
        );
    }
}
