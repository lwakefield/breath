import { h, Component } from 'preact'
import { Link } from 'preact-router'
import give from 'zay/give'
import get from 'lodash/get'
import dateFormat from 'dateformat'
import { bind } from 'decko'
import * as d3 from 'd3'

import BarChart from 'lib/bar-chart'
import Tip from 'lib/tip'
import { loadSites } from 'actions/sites'
import { loadProfiles } from 'actions/profile'
import { loadMetrics } from 'actions/metrics'
import { formatMillis } from 'lib/time'
import { formatBytes } from 'lib/size'
import styles from './styles'

@give(p => ({
    site: `sites/${p.id}`,
    lastProfile: `siteProfiles/${p.id}.0`
}))
export default class Site extends Component {
    componentDidMount () {
        loadSites()
        loadProfiles(
            this.props.id,
            {metrics: 'har-total-transferred-size,har-on-load', limit: 100}
        )
    }

    formatElapsed ({bin, value}) {
        return value.toFixed(2) + 'ms - ' + dateFormat(bin, 'hh:MMtt dd mmm yy')
    }

    formatBytes ({bin, value}) {
        return (value / 1024).toFixed(2) + 'kB - ' + dateFormat(bin, 'hh:MMtt dd mmm yy')
    }

    render ({ site, lastProfile }) {
        if (!site) return null

        const { hostname } = site && new URL(site.url)

        const fullyLoaded = formatMillis(
            get(lastProfile, 'metrics.har-on-load', 0)
        )
        const totalTransferredSize = formatBytes(
            get(lastProfile, 'metrics.har-total-transferred-size', 0)
        )

        return (
            <div class={styles.site}>
                <h2>{ hostname }</h2>
                <div class={styles.summaryList}>
                    <div class={styles.summary}>
                        Load Time: <strong>&nbsp;{fullyLoaded}</strong>
                    </div>
                    <div class={styles.summary}>
                        Data Transferred: <strong>&nbsp;{totalTransferredSize}</strong>
                    </div>
                </div>

                <div class={styles.metricsContainer}>
                    <div class={styles.metricContainer}>
                        <h3 class={styles.metricHeading}>Loaded (ms)</h3>
                        <Metrics siteId={site.id} metric='har-on-load' label={this.formatElapsed} />
                    </div>

                    <div class={styles.metricContainer}>
                        <h3 class={styles.metricHeading}>Transferred (kB)</h3>
                        <Metrics siteId={site.id} metric='har-total-transferred-size' label={this.formatBytes} />
                    </div>
                </div>

                <ProfileHistory siteId={site.id} />
            </div>
        )
    }
}

@give(p => ({
    metrics: `siteMetrics/${p.siteId}.${p.metric}`
}))
class Metrics extends Component {
    componentDidMount () {
        loadMetrics(
            this.props.siteId,
            this.props.metric
        )
    }

    componentDidUpdate () {
        this.renderChart()
    }

    @bind
    mountChart (ref) {
        const tip = new Tip({
            format: this.props.label
        })
        this.chart = new BarChart({
            target: ref,
            height: 200,
            mouseover: tip.show,
            mouseout: tip.hide,
            barPadding: 1,
            margin: {top: 0, left: 0, bottom: 35, right: 0},
            type: 'rect',
            color: ['#83AFE5', '#DF8C8C']
        })
        this.renderChart()
    }

    renderChart () {
        if (!this.props.metrics) return
        if (!this.chart) return

        const data = this.props.metrics.map(v => {
            return { bin: new Date(v.timestamp), value: v.value }
        }).reverse()
        const [min, max] = d3.extent(data, v => v.bin)
        this.chart.xDomain = [min, d3.timeHour.offset(max, 1)]
        this.chart.numXTicks = data.length / 12
        this.chart.width = data.length * 5

        this.chart.render(data)
    }

    render ({ metrics }) {
        if (!metrics) return null

        return (
            <div class={styles.metrics}>
                <svg ref={this.mountChart} class={styles.chart} height="200" />
            </div>
        )
    }
}

@give(p => ({
    profiles: `siteProfiles/${p.siteId}`
}))
class ProfileHistory extends Component {
    render ({ profiles }) {
        if (!profiles) return null

        return (
            <div>
                <h3>History</h3>
                <div class={styles.historyListHeader}>
                    <strong>Timestamp</strong>
                    <strong>Load Time</strong>
                    <strong>Data Transferred</strong>
                </div>
                {
                    profiles.map(p => 
                        <Link href={`/profiles/${p.id}`} class={styles.historyListItem}>
                            <div>{dateFormat(p.created_at, 'd mmm yy h:MM TT')}</div>
                            <div>{formatMillis(get(p, 'metrics.har-on-load'))}</div>
                            <div>{formatBytes(get(p, 'metrics.har-total-transferred-size'))}</div>
                        </Link>
                    )
                }
            </div>
        )
    }
}
