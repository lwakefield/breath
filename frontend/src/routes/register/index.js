import { h, Component } from 'preact';
import { route } from 'preact-router'
import { bind } from 'decko'

import { register } from 'actions/auth'

import styles from './styles';

export default class Register extends Component {
    refs = {}

    @bind
    mountRef (name) {
        return c => this.refs[name] = c
    }

    @bind
    tryRegister (e) {
        e.preventDefault()
        e.stopPropagation()
        register({
            email: this.refs.email.value,
            password: this.refs.password.value,
        }).then(v => route('/sites'))
    }

    render() {
        return (
            <div class={styles.register}>
                <form onSubmit={this.tryRegister}>
                    <h2>Register</h2>
                    <div class={styles.fields}>

                        <input
                            type="email"
                            placeholder="email"
                            ref={this.mountRef('email')}
                        />
                        <input
                            type="password"
                            placeholder="password"
                            ref={this.mountRef('password')}
                        />
                        <button>Register</button>

                    </div>
                </form>
            </div>
        );
    }
}
