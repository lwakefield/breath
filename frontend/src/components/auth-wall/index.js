import { h, Component } from 'preact';
import {route} from 'preact-router'

export default class AuthWall extends Component {
	componentWillMount () {
		if (!localStorage.authToken) {
			route('/login')
		}
	}

	render ({ route: Route, ...rest }) {
		if (!localStorage.authToken) {
			return null
		}

		return <Route {...rest} />
	}
}
