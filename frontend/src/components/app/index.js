import { h, Component } from 'preact';
import { Router } from 'preact-router';

import AuthWall from 'components/auth-wall';
import Sidebar from 'components/sidebar';

import Home from 'routes/home';
import Login from 'routes/login'
import Register from 'routes/register'
import Sites from 'routes/sites'
import Site from 'routes/site'
import Profile from 'routes/profile'

import styles from './styles'

export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div class={styles.app}>
				<Sidebar />

				<div class={styles.wrapper}>
					<main>
						<Router onChange={this.handleRoute}>
							<AuthWall route={Sites}    path="/"             />
							<AuthWall route={Sites}   path="/sites"        />
							<AuthWall route={Site}    path="/sites/:id"    />
							<AuthWall route={Profile} path="/profiles/:id" />

							<Login path='/login' />
							<Register path='/register' />
						</Router>
					</main>
				</div>
			</div>
		);
	}
}
