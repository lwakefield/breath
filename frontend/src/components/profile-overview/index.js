import { h, Component } from 'preact'
import give from 'zay/give'
import values from 'lodash/values'
import mapValues from 'lodash/mapValues'
import sum from 'lodash/sum'
import round from 'lodash/round'

import styles from './styles'

@give(p => ({
    entries: `profileHars/${p.id}.log.entries`,
    page: `profileHars/${p.id}.log.pages.0`,
}))
export default class ProfileOverview extends Component {
    render ({ entries, page }) {

        if (!entries || !page) return null

        const filteredEntries = entries.filter(v => v.time > 0)

        const totalTransferredSize = sum(filteredEntries.map(v => v.response._transferSize))
        const { onContentLoad = 0, onLoad = 0} = page.pageTimings || {}

        const entriesByType = filteredEntries.reduce(
            (a, v) => {
                const type = getResponseType(v)

                if (!a[type]) {
                    a[type] = 1
                } else {
                    a[type]++
                }

                return a
            },
            {}
        )

        const entriesByKBytes = entries.reduce(
            (a, v) => {
                const type = getResponseType(v)

                if (!a[type]) {
                    a[type] = (v.response._transferSize / 1024)
                } else {
                    a[type] += (v.response._transferSize / 1024)
                }

                return a
            },
            {}
        )
        const entriesByKBytesRounded = mapValues(entriesByKBytes, v => round(v, 2))

        const metrics = [
            ['Page Content Loaded', onContentLoad.toFixed(2), 'ms'],
            ['Page Loaded', onLoad.toFixed(2), 'ms'],
            ['# of Requests', filteredEntries.length, '', entriesByType],
            ['Total Transfer Size', (totalTransferredSize / 1024).toFixed(2), 'KB', entriesByKBytesRounded],
        ]

        return (
            <div class={styles.metricList}>
                {
                    metrics.map(([name, val, units, breakdown]) =>
                        <Metric
                            name={name}
                            value={val}
                            units={units}
                            breakdown={breakdown}
                        />
                    )
                }
            </div>
        )
    }
}

function Metric ({name, value, units, breakdown}) {
    return (
        <div class={styles.metric}>
            <div class={styles.metricName}>{name}:</div>
            <strong>{value}{units}</strong>
            { breakdown && <Breakdown {...breakdown} units={units}/> }
        </div>
    )
}

const types = [
    'html',
    'css',
    'javascript',
    'json',
    'image',
    'font'
]

function Breakdown ({units, ...props}) {
    const total = sum(values(props))
    const available = types.filter(v => props[v])
    const toggleKeys = () => this.setState({showKeys: !this.state.showKeys})
    const stop = e => e.stopPropagation()

    return (
        <div class={styles.metricBreakdownWrapper} onClick={toggleKeys}>
            <div class={styles.metricBreakdown}>
                {available.map(v => 
                    <div
                        class={styles[v]}
                        style={{width: (100 * props[v] / total) + '%'}}
                    />
                )}
            </div>

            {this.state.showKeys && <div class={styles.keyList}>
                {
                    available.map(v =>
                        <div class={styles.key} onClick={stop}>
                            <div class={styles[v]} />
                            <span class={styles.keyLeft}>
                                {`${props[v]}${units}`}
                            </span>
                            <span class={styles.keyRight}>{v}</span>
                        </div>
                    )
                }
            </div>}
        </div>
    )
    return total
}

function findResponseHeader (entry, name) {
    return entry.response.headers
        .find(v => v.name.toLowerCase() === name.toLowerCase())
}

function getResponseType (entry) {

    const contentType = findResponseHeader(entry, 'Content-Type')
    if (!contentType) return 'other'

    const mappedType = types.find(v => contentType.value.toLowerCase().indexOf(v) !== -1)
    return mappedType || 'other'
}
