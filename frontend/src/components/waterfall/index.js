import { h, Component } from 'preact'
import give from 'zay/give'
import get from 'lodash/get'
import times from 'lodash/times'

import styles from './styles'

@give(props => ({
    entries: `profileHars/${props.id}.log.entries`,
    timings: `profileHars/${props.id}.log.pages.0.pageTimings`,
}))
export default class Waterfall extends Component {
    render ({entries = [], timings = {}}) {
        const totalTime = Math.max(getTotalTime(entries), timings.onLoad)

        const onContentLoadPercent = 100 * (timings.onContentLoad / totalTime)
        const onLoadPercent        = 100 * (timings.onLoad / totalTime)

        const numSecs = Math.max(Math.floor(totalTime / 1000), 0)
        const secOffset = 100 * (1000 / totalTime)

        return (
            <div class={styles.graph}>
                <div class={styles.graphContent}>
                    <div class={styles.background}>
                        <div class={styles.backgroundPadding} />
                        <div class={styles.backgroundContent}>
                            {times(numSecs + 1, v =>
                                <div
                                    class={styles.markerGray}
                                    style={{left: (v * secOffset) + '%'}}
                                />
                            )}

                            <div class={styles.markerBlue} style={{left: onContentLoadPercent + '%'}}/>
                            <div class={styles.markerRed} style={{left: onLoadPercent + '%'}}/>
                        </div>
                    </div>

                    <div class={styles.header}>
                        <div class={styles.headerLeft}>
                            Resource Url
                        </div>
                        <div class={styles.headerRight}>
                            {times(numSecs+1, v =>
                                <div
                                    class={styles.headerSecondMarker}
                                    style={{left: (v * secOffset) + '%'}}
                                >{v}s
                                </div>
                            )}
                        </div>
                    </div>

                    {entries.map((v, k) => v.time > 0
                        ? <Entry
                            key={k}
                            profileId={this.props.id}
                            entryId={k}
                        />
                        : null
                    )}
                </div>
            </div>
        )
    }
}

@give(props => ({
    entries: `profileHars/${props.profileId}.log.entries`,
    entry:   `profileHars/${props.profileId}.log.entries.${props.entryId}`,
    timings: `profileHars/${props.profileId}.log.pages.0.pageTimings`,
}))
class Entry extends Component {
    state = {expanded: false}

    toggleExpanded = () => this.setState({expanded: !this.state.expanded})

    render ({entries, entry, timings}) {
        const startTime = getStartTime(entries)
        const totalTime = getTotalTime(entries)

        const getWidth  = v => 100 * v.time / totalTime
        const getTime   = v => (new Date(v.startedDateTime)).getTime()
        const getMargin = v => 100 * (getTime(v) - startTime) / totalTime

        return (
            <div class={styles.entry}>
                <div class={styles.entryTop} onClick={this.toggleExpanded}>
                    <div class={styles.entryName}>
                        {getResourceName(entry)}
                    </div>
                    <div class={styles.timeBlockWrapper}>
                        <TimeBlock
                            entry={entry}
                            style={{
                                marginLeft: `${getMargin(entry)}%`,
                                width: `${getWidth(entry)}%`
                            }}
                        />
                    </div>
                </div>
                {this.state.expanded && <pre>{JSON.stringify(entry, null, '    ')}</pre>}
            </div>
        )
    }
}

function getTotalTime (entries) {
    return getEndTime(entries) - getStartTime(entries)
}

function getStartTime (entries) {
    const times = entries.map(v => (new Date(v.startedDateTime)).getTime())
    return Math.min(...times)
}

function getEndTime (entries) {
    const times = entries.map(v => {
        return (new Date(v.startedDateTime)).getTime() + v.time
    })
    return Math.max(...times)
}


function getResourceName (entry) {
    const url = new URL(entry.request.url)
    const {pathname, hostname, search} = url
    return pathname === '/' ? hostname : pathname+search
}

function TimeBlock ({style, entry}) {
    const { time } = entry
    const getWidth = v => 100 * v / time
    const order = [
        'blocked',
        'dns',
        'connect',
        'ssl',
        'send',
        'wait',
        'receive'
    ]
    const toShow = order.filter(v => entry.timings[v] > 0)
    return (
        <div class={styles.timeBlock} style={style}>
            {
                toShow.map(k =>
                    <div 
                        class={styles[k]}
                        style={{width: `${getWidth(entry.timings[k])}%`}}
                    />
                )
            }
        </div>
    )
}
