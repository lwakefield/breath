import { h, Component } from 'preact';
import { Link } from 'preact-router'
import classnames from 'classnames'
import { bind } from 'decko'

import styles from './styles'

export default class Sidebar extends Component {

    toggleDrawer = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    }

    @bind
    logout () {
        localStorage.removeItem('authToken')
        window.location.replace(window.location.origin)
    }

    render() {
        return (
            <div class={styles.sidebar}>
                <div class={styles.header}>
                    <Link href="/"><h1>Breath</h1></Link>

                    <button onClick={this.toggleDrawer}>
                        {this.state.drawerOpen ? '×' : '☰'}
                    </button>
                </div>

                <div class={classnames(
                    styles.drawer,
                    {[styles.drawerOpen]: this.state.drawerOpen}
                )}>
                    <div />

                    <div
                        onClick={this.logout}
                        class={styles.logout}
                        children={['Log out']}
                    />
                </div>
            </div>
        );
    }
}
