const http       = require('http')
const express    = require('express')
const bodyParser = require('body-parser');
const handler    = require('./handler')

const app = express()
app.use(bodyParser.text({type: '*/*'}));

app.post('/', async (request, response) => {
  runLighthouse(request).then(res => {
    response.statusCode = res.statusCode
    response.setHeader('Content-Type', 'application/json')
    response.end(res.body)
  })
})

async function runLighthouse (request) {
  return new Promise((resolve, reject) => {
    handler.lighthouse(request, null, (err, res)  => {
      if (err) reject(err)
      resolve(res)
    })
  })
}

const port = process.env.PORT
app.listen(port, function () {
  console.log(`listening on port ${port}`)
})
