'use strict';

const { exec, execSync } = require('child_process')
const fs = require('fs')
const entries = require('lodash/entries')
const values = require('lodash/values')
const sum = require('lodash/sum')
const { harFromMessages } = require('chrome-har')
const {sync: ls} = require('glob')
const AWS = require('aws-sdk')

const config = require('./config')
const {LIGHTHOUSE_FLAGS, LIGHTHOUSE_PATH, TMP_PATH} = config

const s3 = new AWS.S3(config.AWS_CONFIG)

module.exports.default = (event, context, callback) => {
  const {id, url} = JSON.parse(event.body)
  console.log(id, url);
  if (!id || !url) {
    return callback(null, { statusCode: 400 })
  }

  runLighthouse({id, url})
    .then(() => {
      const har = makeHar(id)
      fs.writeFileSync(`${TMP_PATH}/${id}.har`, JSON.stringify(har))
    }).then(() => {
      const promises = ls(`${TMP_PATH}/${id}*`)
        .map(path => {
          const Key = path.replace(`${TMP_PATH}/`, '')
          const Body = fs.readFileSync(path)
          const ContentType = 'application/json'
          const Bucket = config.S3_BUCKET

          return s3.upload({Body, Bucket, Key, ContentType}).promise()
        })
      return Promise.all(promises)
    }).then(() => {
      const metrics = getMetrics(id)
      execSync(`rm -rf ${TMP_PATH}/*`)

      callback(null, { statusCode: 200, body: JSON.stringify(metrics) })
    })
};

function runLighthouse ({id, url}) {
  const lighthouseFlags = [
    ...LIGHTHOUSE_FLAGS,
    `--output-path=${TMP_PATH}/${id}.json`
  ]
  const cmd = [LIGHTHOUSE_PATH, ...lighthouseFlags, url]

  return new Promise((resolve, reject) => {
    const ps = exec(cmd.join(' '), {stdio: [0, 1, 2]})
    ps.stdout.on('data', v => console.log(v.toString()))
    ps.stderr.on('data', v => console.error(v.toString()))
    ps.on('close', code => {
      if (code !== 0) reject('Non-zero exit code')
      resolve()
    })
  })
}

function makeHar (id) {
  const devToolsLog = readJSONFile(`${TMP_PATH}/${id}-0.devtoolslog.json`)
  const harData = harFromMessages(devToolsLog)

  return harData
}

function getMetrics (id) {
  const lighthouseResults = readJSONFile(`${TMP_PATH}/${id}.json`)
  const har = readJSONFile(`${TMP_PATH}/${id}.har`)

  const lighthouseMetrics = [
    'first-meaningful-paint',
    'first-interactive',
    'consistently-interactive',
    'speed-index-metric',
    'estimated-input-latency',
    'total-byte-weight',
    'dom-size',
  ].reduce((acc, curr) => {
    const key = `lighthouse-${curr}`
    const val = lighthouseResults.audits[curr].rawValue
    return Object.assign(acc, {[key]: val})
  }, {})


  const harPage = har.log.pages[0]
  const harMetrics = {
    'har-on-content-load': harPage.pageTimings.onContentLoad,
    'har-on-load': harPage.pageTimings.onLoad,
  }
  const harSizeMap = har.log.entries.reduce(
    (map, entry) => {
      const { mimeType } = entry.response.content
      const transferredSize = Math.max(entry.response.bodySize, 0)

      const accumulatedSize = (map[mimeType] || 0) + transferredSize
      const key = `har-transferred-size.${mimeType}`
      return Object.assign(map, {[key]: accumulatedSize})
    },
    {}
  )
  const totalTransferredSize = sum(values(harSizeMap))

  return Object.assign(
    {},
    {'lighthouse-score': lighthouseResults.score},
    lighthouseMetrics,
    harMetrics,
    harSizeMap,
    {'har-total-transferred-size': totalTransferredSize}
  )
}

function readJSONFile (path) {
  const buffer = fs.readFileSync(path)
  const json = JSON.parse(buffer.toString())
  return json
}
