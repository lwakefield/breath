Running the server:
```
TMP_PATH=./tmp PORT=8080 nodemon --ignore tmp dev-server.js
```

Making a request:
```
curl -X POST -d '{"id": "4", "url": "https://clidemo.preactjs.com/"}' localhost:8080
```
