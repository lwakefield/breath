const { tmpdir } = require('os')

// See: https://github.com/adieuadieu/serverless-chrome/commit/8d69a25686e4010e6d808783a89472f0d583988b
const CHROME_FLAGS = [
  '--no-sandbox',
  '--disable-gpu',
  '--headless',
  '--single-process',
  '--no-zygote',
  '--enable-logging',
  '--log-level=0',
  '--v=99'
]
const LIGHTHOUSE_FLAGS = [
    `--chrome-flags="${CHROME_FLAGS.join(' ')}"`,
    '--output=json',
    '--save-assets',
    '--save-artifacts',
]
const LIGHTHOUSE_PATH         = 'node_modules/lighthouse/lighthouse-cli/index.js'
const TMP_PATH                = process.env.TMP_PATH || tmpdir()
const AWS_ACCESS_KEY_ID       = process.env.AWS_ACCESS_KEY_ID
const AWS_SECRET_ACCESS_KEY   = process.env.AWS_SECRET_ACCESS_KEY
const AWS_ENDPOINT            = process.env.AWS_ENDPOINT
const AWS_S3_FORCE_PATH_STYLE = process.env.AWS_S3_FORCE_PATH_STYLE || false
const S3_BUCKET               = process.env.S3_BUCKET

const useAwsConfig = AWS_ACCESS_KEY_ID
  && AWS_SECRET_ACCESS_KEY
  && AWS_ENDPOINT
  && AWS_S3_FORCE_PATH_STYLE
const AWS_CONFIG = useAwsConfig 
  ? {
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    endpoint: AWS_ENDPOINT,
    s3ForcePathStyle: AWS_S3_FORCE_PATH_STYLE
  }
  : undefined

module.exports = {
  CHROME_FLAGS,
  LIGHTHOUSE_FLAGS,
  LIGHTHOUSE_PATH,
  TMP_PATH,
  S3_BUCKET,
  AWS_CONFIG
}
